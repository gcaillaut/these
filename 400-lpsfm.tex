\chapter{Une approche gloutonne pour l'apprentissage supervisé d'espaces prétopologiques}\label{chap-lpsfm}

L'objectif de ce chapitre est de présenter un algorithme d'apprentissage supervisé d'espaces prétopologiques. Le but ultime est alors d'apprendre un espace prétopologique dans le but de l'utiliser comme un modèle prédictif. Cette approche est assez originale en soi puisque cette piste ne semble avoir été explorée que par \textcite{DBLP-conf/pkdd/CleuziouD15} puis améliorée par \textcite{DBLP-books/hal/Cleuziou15}. Dans ces travaux, les auteurs ont pour objectif de mettre en place un système automatique d'extraction de taxonomies lexicales. Ce cadre de travail particulier leur permet d'émettre un certain nombre d'hypothèses, notamment en ce qui concerne la forme de la structure prétopologique à apprendre. Cette connaissance supplémentaire est indispensable puisque LPS ne dispose que d'informations partielles sur les caractéristiques, les fermés élémentaires, du modèle à apprendre. L'algorithme LPS proposé est alors guidé par une heuristique favorisant les modèles dont la structuration par l'algorithme de \textcite{DBLP-journals/isci/LargeronB02} s'approche de celle d'un arbre.

% \textcite{DBLP-conf/pkdd/CleuziouD15} présentent LPS, un algorithme d'apprentissage d'espaces prétopologiques, dans le but d'extraire automatiquement des taxonomies lexicales. Dans ce contexte, LPS reçoit en entrée un ensemble \( E \) de termes à structurer, un ensemble \( \mathcal{N} \) de relations de voisinages, typiquement des relations binaires, ainsi qu'une structure \( S^* \) d'une taxonomie lexicale de référence. L'algorithme cherche alors à combiner les relations de voisinages, sous la forme d'une formule logique \( Q \) en forme normale disjonctive sans négation, de sorte à ce que la structuration des fermés élémentaires de l'espace prétopologique \( (E, a_Q) \) ressemble à \( S^* \).

% Une autre façon de voir les choses et de considérer la fonction \( S^* : E \rightarrow \PE \) qui associe à chaque élément \( x \) de \( E \) son fermé élémentaire. L'objectif consiste alors à apprendre l'espace prétopologique \( (E, a_Q) \) tel que sa fonction de fermeture \( F_Q \) donne les même fermés élémentaires que \( S^* \).

% \begin{equation*}
%   \forall x \in E,\, F(\{ x \}) = S^*(x)
% \end{equation*}

% On dit qu'un élément \( x \) de \( E \) est en relation avec un autre élément \( y \) de \( E \), selon la structure de référence \( S^* \), si et seulement si \( y \) appartient au fermé élémentaire cible de \( x \), c'est à dire \( S^*(x) \).

% \medskip

% En pratique, \citeauthor{DBLP-conf/pkdd/CleuziouD15} procèdent différemment, pour deux raisons. D'une part, ils proposent d'apprendre un modèle à partir d'une structure de référence incomplète. Ce choix est motivé par la difficulté à construire une taxonomie lexicale fiable sur un domaine particulier. C'est pourquoi ils se sont tournés vers une solution automatique. La structure de référence en entrée de leur algorithme est en réalité la structure retournée par une méthode d'extraction à base de patrons lexicaux~\autocite{kozareva2010semi}. Les patrons lexicaux sont réputés pour capturer de façon fiable les relations d'hyperonymie et d'hyponymie entre les termes d'un corpus. C'est à dire que si le patron << \( x \) est un \( y \) >> est repéré dans le corpus, alors il est hautement probable que le terme \( x \) soit bien un hyponyme du terme \( y \). En contrepartie, cette approche est assez peu couvrante, les relations issues d'un tel processus d'extraction sont alors peu fournies.

% Les relations extraites par l'approche par patrons syntaxiques servent donc de \emph{référence partielle}. C'est à dire que s'il existe une relation d'un terme \( x \) vers un terme \( y \), alors \( y \) doit appartenir au fermé élémentaire de \( x \). Autrement dit, si \( y \) est dans le \emph{fermé partiel cible} \( S^*(x) \), alors, idéalement, \( y \) doit aussi appartenir à \( F_Q(\{ x \}) \). D'autre part, si une telle relation de \( x \) vers \( y \) existe dans \( S^* \) alors retrouver la relation inverse, c'est à dire \( x \) appartient à \( F_Q(\{ y \}) \), est considéré comme une erreur. En revanche, si \( y \) n'appartient pas à \( S^*(x) \), rien n'indique que \( y \) soit ou ne soit pas un hyponyme de \( x \).

% L'algorithme LPS d'apprentissage est guidé par un score de correspondance entre les fermés élémentaires de l'espace prétopologique \( (E, a_Q) \) à évaluer et les relations présentes dans la référence partiel \( S^* \). C'est la F-mesure qui a été choisie pour sa capacité à résumer en un score la fiabilité (précision) et la couverture (rappel) d'un modèle. On note \( E_{S^*} \) l'ensemble des éléments de \( E \) participant aux relations dans \( S^* \).

% La précision est définie comme étant le ratio entre le nombre de relations correctes capturées par le modèle par rapport au nombre total de relations capturées par le modèle. Le rappel est défini comme le ratio entre le nombre de relations correctes capturées par le modèle par rapport au nombre de relations présentes dans la structure cible. Dans les deux cas, seules les relations entre deux éléments de \( E_{s^*} \) sont considérées.

% \begin{align*}
%   \precision(Q, S^*) &= \frac{\sum_{x \in E_{S^*}} \lvert S^*(x) \cap F_Q(\{ x \}) \rvert}{\sum_{x \in E_{S^*}} \lvert F_Q(\{ x \}) \rvert} \\
%   \recall(Q, S^*) &= \frac{\sum_{x \in E_{S^*}} \lvert S^*(x) \cap F_Q(\{ x \}) \rvert}{\sum_{x \in E_{S^*}} \lvert S^*(x) \rvert} \\
%   \fmeasure(Q, S^*) &= 2 \cdot \frac{\precision(Q, S^*) \cdot \recall(Q, S^*)}{\precision(Q, S^*) + \recall(Q, S^*)}
% \end{align*}

% La F-mesure définie de cette façon ne permet de n'évaluer qu'une sous-partie de la structure apprise, puisque l'ensemble d'apprentissage, c'est à dire la structure de référence, est incomplet. De plus, dans le cas où la structure de référence est obtenue par un processus automatique, rien n'assure qu'elle ne contienne pas d'erreurs. C'est pourquoi cette mesure de qualité est insuffisante. \citeauthor{DBLP-conf/pkdd/CleuziouD15} proposent d'intégrer, en plus de la F-mesure, un score d'évaluation structurelle, noté \( I(Q) \). Les auteurs ont observé que les taxonomies lexicales ont plutôt tendances à ressembler à des arbres qu'à des graphes orientés sans cycle. C'est à dire que les n\oe{}uds d'une taxonomie lexicale ont habituellement un unique parent direct, à l'exception de la racine. Par conséquent, l'objectif de ce score structurel et des pénaliser les modèles dont la structuration s'écartent de celle d'un arbre. Le score de structuration, s'exprime alors par :
% \begin{equation*}
%   I(Q) = e^{-(d(Q) - 1)^2}
% \end{equation*}
% où \( d(Q) \) est le nombre moyen de parents directs dans la structure déduite des fermés élémentaires de l'espace prétopologique \( (E, a_Q) \).

% La fonction d'évaluation finale consiste alors en une combinaison de ces deux critères :

% \begin{equation*}
%   h(Q, S^*) = \fmeasure(S_Q, Q^*) \cdot I(S_Q)
% \end{equation*}

Cette heuristique d'évaluation offre la possibilité d'apprendre un modèle prétopologique pour la structuration de taxonomies lexicales à partir d'une structure de référence partielle. L'objectif de cette thèse est, entre autre, d'étendre le champ applicatif de la méthode LPS ; c'est-à-dire d'apprendre des modèles prétopologiques pour des problèmes autres que l'extraction de taxonomies lexicales. L'hypothèse selon laquelle les éléments forment une structure d'arbre ne s'applique pas au cadre général.

Dans le cadre de l'extension de la méthode LPS, il n'est donc pas envisageable de conserver le score de structuration. Bien entendu, dans des cas précis ou la forme de la structure, et donc des fermés élémentaires, est connue, un tel critère peut être intégré. Mais ce n'est pas le cas en général, c'est pourquoi ce critère de structuration doit être retiré pour redéfinir le cadre dans lequel LPS s'applique.

Or ce critère de structuration est nécessaire lorsque la qualité du modèle ne peut être calculée qu'à partir d'une structure de référence partielle. Une solution est de ne travailler qu'avec des structures de référence complètes, telles que la présence d'une relation engendre une instance positive et son absence une instance négative. C'est cette approche qui a été choisie dans le cadre de cette thèse.

\medskip

Hormis les difficultés liées à la formulation du problème d'apprentissage d'espaces prétopologiques, l'algorithme LPS tel que proposé par \textcite{DBLP-conf/pkdd/CleuziouD15} possède certains défauts liés à la méthode d'apprentissage génétique sur laquelle repose LPS. En effet, les approches évolutionnaires requièrent de définir un certain nombre de paramètres, tels que la taille de population initiale ou le taux de mutation. Or l'effet qu'auront certaines valeurs sur le processus d'apprentissage est assez difficiles à prédire, aussi bien pour un néophyte qu'un expert.

D'autre part, les algorithmes génétiques reposent sur un processus stochastique pour générer les populations de chaque itération. Par conséquent, un même jeu de paramètres peut produire différents résultats. On peut cependant limiter l'impact de ce processus aléatoire en attribuant un grand nombre à la taille de la population initiale. Or la complexité de ce genre d'algorithme est dépendante de la taille de la population, l'augmenter augmente donc substantiellement le temps d'exécution de l'algorithme.

LPS, dans sa forme génétique, ou évolutionnaire, est alors une méthode compliquée à mettre en place et dont la complexité en temps est élevée. Le second objectif consiste alors à simplifier l'utilisation de LPS.

\medskip

La suite de ce chapitre consiste à présenter un nouveau cadre de travail, plus simple et plus générique, pour l'algorithme LPS. 

\section{LPS glouton}

Dans la suite de ce document, une distinction est faite entre l'algorithme LPS d'apprentissage proposé par \textcite{DBLP-conf/pkdd/CleuziouD15} et la version gloutonne décrite dans cette section. Ainsi, \emph{LPS Génétique} désignera la variante de LPS reposant sur un algorithme génétique et dont la sortie est un espace prétopologique définit par une formule logique \( Q \) en forme normale disjonctive. S'il est nécessaire de distinguer les approches numériques, par apprentissage d'un vecteur \( \bm{w} \), et logiques, on qualifiera LPS Génétique de \emph{numérique} ou de \emph{logique}. L'approche présentée dans cette section sera appelée \emph{LPS Glouton}.

\medskip

On considère un ensemble \( E \) d'éléments, une fonction cible \( S^* : E \rightarrow \PE \) de fermeture élémentaire et un ensemble \( \mathcal{Q} \) de prédicats. L'objectif de LPS Glouton est d'apprendre une formule logique \( Q \) en forme normale disjonctive composée des prédicats de \( \mathcal{Q} \), sans négation. De plus, les fermés élémentaires de l'espace prétopologique \( (E, a_Q) \) doivent se rapprocher des fermés élémentaires cibles décrits par \( S^* \).

Dans le chapitre précédent, les prédicats fournis en entrée de LPS Génétique étaient construits autour de fonctions de voisinages. Nous considérons ici un cadre plus général dans lequel un prédicat est une fonction associant une valeur Booléenne à toute paire \( (A, x) \) de \( \PE \times E \), sans plus de restrictions.

\begin{equation*}
  \forall A \in \PE,\, \forall x \in E,\, q(A, x) = \begin{cases}
    1 & \text{si } A \text{ est en relation avec } x \\
    0 & \text{sinon}
  \end{cases}
\end{equation*}

Le champ applicatif de LPS Glouton ne se limite alors plus nécessairement aux espaces prétopologiques de type V. Le type de l'espace prétopologique en sortie est uniquement dépendant de la formule logique \( Q \) apprise et des prédicats qui la composent. On peut cependant forcer LPS Glouton à apprendre un espace prétopologique de type V en ne lui fournissant que des prédicats respectant l'isotonie :

\begin{equation*}
  \forall A \in \PE,\, \forall B \in \PE,\, \forall x \in E,\, A \subseteq B \Rightarrow q(A, x) \le q(B, x)
\end{equation*}

\begin{property}
  Soit \( E \) un ensemble d'éléments et \( Q \) une formule logique en forme normale disjonctive dépourvue de négation. Si \( Q \) n'est constitué que de prédicats respectant l'isotonie, alors l'espace prétopologique \( (E, a_Q) \) est de type V.
\end{property}

\begin{proof}
  Montrer que l'espace prétopologique \( (E, a_Q) \) est de type V revient à montrer que \( Q \) respecte l'isotonie. Soit deux parties \( A \) et \( B \) de \( E \) telles que \( A \subseteq B \). Montrons que, pour tout élément \( x \) de \( E \), \( Q(A, x) \le Q(B, x) \).

  Si \( Q(A, x) \) est faux, alors peu importe la valeur de \( Q(B, x) \). Si \( Q(A, x) \) est vrai, cela signifie qu'il existe une clause conjonctive dans \( Q \) dont tous les littéraux \( q(A, x) \) sont vrais. Puisqu'ils respectent l'isotonie, ils sont également vrais pour \( Q(B, x) \). Par conséquent, si \( Q(A, x) \) est vrai, \( Q(B, x) \) l'est aussi.
\end{proof}

Le principe de LPS Glouton est de construire une formule logique de façon incrémentale, ou gloutonne, clause conjonctive par clause conjonctive. L'algorithme débute avec une formule logique \( Q \) vide, puis une nouvelle clause est insérée à la suite de chaque itération de l'algorithme, comme illustré en \cref{fig-lps-algorithm}. Ce processus continue tant que le critère d'arrêt, spécifié par l'utilisateur, n'est pas atteint. De fait, la taille, en nombre de clauses conjonctives, de la formule logique n'est pas fixée a priori, contrairement à LPS Génétique à qui il est nécessaire de fournir la taille de la formule logique.

\begin{figure}
  \centering
  \includefigure[width=\linewidth]{lps-algorithm}
  \caption{L'algorithme LPS Glouton.}\label{fig-lps-algorithm}
\end{figure}

Le comportement de LPS Glouton est régi par trois paramètres :
\begin{itemize}
  \item un critère d'arrêt ;
  \item une fonction d'évaluation ;
  \item une stratégie de sélection des clauses conjonctives candidates à l'insertion.
\end{itemize}

Même si d'autre critères peuvent être envisagés, on considérera que l'algorithme s'arrête lorsque le nombre maximal autorisé d'itérations est atteint ou lorsque le score du modèle diminue ou stagne suite à l'insertion d'une nouvelle clause conjonctive.

\begin{algorithm}
  \Fn{LPSGlouton (E, \( S^* \), \( \mathcal{Q} \), \( maxiter \), \( faisceau \))}{
    \( iter \gets 0 \) \;
    \( score \gets 0 \) \;
    \( stop \gets faux \) \;
    \( Q \gets \emptyset \)\label{line-greedy-init} \;

    \Tq{\( iter < maxiter \) et \( \neg stop \)}{
      \( iter \gets iter + 1 \) \;
      \( clause^* \gets MeilleurClause(E, S^*, \mathcal{Q}, Q, \emptyset, faisceau) \)\label{line-greedy-find-best} \;
      \( Q^{\prime} \gets Q \vee clause^* \) \;
      % \( S_{Q^{\prime}} \gets structuring(E, Q^{\prime}) \) \;

      \Si{\( clause = \emptyset \) ou \( evaluation(Q^{\prime}, S^*) \le score \)}{
        \( stop \gets true \) \;
      }
      \Sinon{
        \( score \gets evaluation(Q^{\prime}, S^*) \) \;
        \( Q \gets Q^{\prime} \) \;
      }
    }

    \Retour{\( Q \)}
  }

  %\TitleOfAlgo{LPS Glouton}
  \caption{LPS Glouton}\label{algo-greedy}
\end{algorithm}

Le pseudo-code le l'algorithme LPS Glouton est présenté en \cref{algo-greedy}. L'algorithme débute avec une formule logique \( Q \) vide (\cref{line-greedy-init}). Des clauses conjonctives sont ensuite ajoutées à \( Q \) au fil des itérations. Au cours de chaque itération, la clause \( clause^* \) conjonctive maximisant la qualité de la formule logique \( Q^{\prime} = Q \vee clause^* \) est recherchée (\cref{line-greedy-find-best}). Si \( Q^{\prime}\) obtient un meilleur score que \( Q \), alors \( Q \) est remplacée par \( Q^{\prime} \) et l'itération suivante débute, sinon l'algorithme s'arrête car il n'a pas trouvé de clause permettant d'augmenter la qualité de la formule logique courante.

\subsection{Fonction d'évaluation}

La fonction d'évaluation permet à LPS Glouton d'évaluer la qualité d'un modèle en cours d'apprentissage. Cette fonction est cruciale puisque c'est principalement cette fonction qui guide l'algorithme dans son processus d'apprentissage.

En théorie, la fonction d'évaluation est censée formaliser analytiquement le but recherché par l'algorithme d'apprentissage. Ici, l'algorithme cherche à apprendre un espace prétopologique dont les fermés sont similaires à ceux retournés par la fonction \( S^* \) de référence. Par conséquent, la fonction d'évaluation doit permettre de quantifier la correspondance entre les fermés élémentaires d'un modèle \( (E, a_Q) \) en cours d'apprentissage et les fermés élémentaires cibles décrits par \( S^* \). En pratique, la qualité de l'espace prétopologique \( (E, a_Q) \) sera estimé par la F-mesure de ses fermés élémentaires par rapport à \( S^* \).

\begin{align}\label{eqn-prec-rec-fm}
  \precision(Q, S^*) &= \frac{\sum_{x \in E} \lvert S^*(x) \cap F_Q(\{ x \}) \rvert}{\sum_{x \in E} \lvert F_Q(\{ x \}) \rvert} \\
  \recall(Q, S^*) &= \frac{\sum_{x \in E} \lvert S^*(x) \cap F_Q(\{ x \}) \rvert}{\sum_{x \in E} \lvert S^*(x) \rvert} \\
  \fmeasure(Q, S^*) &= 2 \cdot \frac{\precision(Q, S^*) \cdot \recall(Q, S^*)}{\precision(Q, S^*) + \recall(Q, S^*)}
\end{align}

La définition de la F-mesure donnée ici diffère de celle donnée précédemment du fait que \( S^* \) est définie sur l'ensemble \( E \) complet. La F-mesure peut alors être définie sur le fait que la présence d'un élément \( y \) dans le fermé élémentaire cible \( S^*(x) \) désigne une instance positive. Au contraire, l'absence d'un élément \( z \) dans le fermé \( S^*(x) \) désigne une instance négative. Les instances positives et négatives sont donc pleinement définies.

La F-mesure est choisie ici car elle semble être un critère de qualité au moins raisonnable dans de nombreux cas. On peut sans aucun doute trouver des exemples dans lesquels la F-mesure n'est pas le critère de qualité le plus pertinent. Dans ces cas particuliers, l'utilisateur est libre d'utiliser le critère d'évaluation qui lui convient. Le \cref{chap-lpsmi} est d'ailleurs dédié à l'élaboration d'un critère d'évaluation spécifiquement adapté à l'apprentissage d'espaces prétopologiques de type V.

\subsection{Stratégie de sélection de la meilleure clause conjonctive}

Le choix d'une fonction d'évaluation est crucial puisqu'il permet d'établir un ordre entre l'ensemble des espaces prétopologiques définis par une formule logique en forme normale disjonctive composée des prédicats de \( \mathcal{Q} \) ; en tenant compte d'une fonction \( S^* \) de fermeture élémentaire de référence.

L'objectif visé par la méthode LPS est tout simplement de trouver la formule logique \( Q^* \) tels que les fermés élémentaires de l'espace prétopologique \( (E, a_{Q^*}) \) soit rigoureusement identiques à ceux décrits par \( S^* \). Le problème étant que l'espace des formules logiques minimales en forme normale disjonctive sans négation est vaste et son exploration difficile. Pour s'en convaincre, il suffit de réduire le problème à la recherche de la meilleure clause conjonctive combinant les prédicats de \( \mathcal{Q} \), ce qui revient à effectuer une itération de l'algorithme LPS Glouton. Toutes les combinaisons des prédicats de \( \mathcal{Q} \) sont alors une solution à ce problème, résoudre cette tâche revient alors à évaluer les \( 2^{\lvert \mathcal{Q} \rvert} \) solutions possibles pour en extraire la meilleure. Ce n'est donc pas une stratégie envisageable.

Il est donc impératif d'utiliser une stratégie de recherche permettant d'explorer efficacement l'ensemble des clauses conjonctives, c'est-à-dire l'ensemble des parties de \( \mathcal{Q} \). On pose la clause conjonctive vide comme point de départ de la stratégie de recherche et on définit le graphe de recherche de sorte que les clauses de taille \( n \) amènent aux clauses de taille \( n + 1 \) plus spécifiques. Par exemple, si on considère l'ensemble \( \mathcal{Q} = \{ q_1, q_2, q_3, q_4 \} \) de prédicats, comme en \cref{fig-strategie-recherche}, alors la clause conjonctive \( q_1 \) permet de visiter les clauses conjonctives de taille 2 et plus spécifiques que \( q_1 \) ; ce qui correspond aux clauses \( q_1 \wedge q_2 \), \( q_1 \wedge q_3 \) et \( q_1 \wedge q_4 \).

\begin{figure}
  \centering
  \includefigure{strategie-recherche}
  \caption{Graphe de recherche de la meilleure clause conjonctive.}\label{fig-strategie-recherche}
\end{figure}

 D'emblée, les algorithmes de type recherche en largeur ou en profondeur sont éliminés, puisque cela nécessiterait de scanner et d'évaluer l'ensemble des solutions. Un algorithme de type séparation et évaluation, ou \emph{branch and bound} en anglais, serait intéressant puisqu'il assurerait d'une part l'optimalité de la solution retournée et permettrait d'autre part de réduire l'espace de recherche en coupant les arêtes du graphe de recherche menant à de mauvaises solutions. Or, il n'est pas évident de déterminer quelles arêtes couper sans explorer le sous-graphe vers lequel elles amènent.

 L'existence d'un algorithme de recherche à la fois efficace et optimal semble compromise. C'est pourquoi le choix de la stratégie de recherche s'est porté sur une stratégie de recherche en faisceau. Les algorithmes de recherche en faisceau appliquent la même stratégie que les algorithmes de recherche en largeur, à la différence que tous les n\oe{}uds ne sont pas explorés. \'{E}tant donné un entier \( k \) représentant la taille du faisceau, l'algorithme évalue toutes les solutions d'un même rang et conserve les \( k \) meilleures. L'itération suivante évaluera uniquement les enfants des \( k \) solutions sélectionnées. L'exemple en \cref{fig-strategie-recherche-selection} illustre le fonctionnement de ce processus de recherche avec un faisceau de taille deux.

 \begin{figure}
  \centering
  \includefigure{strategie-recherche-selection}
  \caption{Recherche avec un faisceau de taille 2 de la meilleure clause conjonctive. Les clauses bleues sont les clauses retenues lors de l'itération en cours et les clauses vertes sont les candidates de l'itération à venir.}\label{fig-strategie-recherche-selection}
\end{figure}

On peut alors étendre ou, au contraire, limiter la recherche de la meilleure solution à plus ou moins de candidates en augmentant ou en diminuant la taille du faisceau. Cette stratégie permet de limiter le nombre de solutions à visiter en se préoccupant uniquement de celles qui semblent les plus pertinentes. Toutefois, la solution retournée par ce type d'algorithme n'est par nécessairement optimale, puisqu'il s'agit d'une alternative moins coûteuse à une stratégie de recherche exhaustive.

\subsection{L'algorithme LPS Glouton}

\begin{algorithm}
  \Fn{MeilleurClause (E, \( S^* \), \( \mathcal{Q} \), \( Q \), \( base \), \( faisceau \))}{
    \Si{\( \left\lvert base \right\rvert \ge \left\lvert \mathcal{Q} \right\rvert \)\label{line-end-rec}}{
      \tcc{\( base \) contient déjà tous les prédicats de \( \mathcal{Q} \)}
      \Retour{\( base \)} \;
    }

    \( clause^* \gets \emptyset \) \;
    \( score^* \gets 0 \) \;

    \( clauses \gets [\ ] \) \;
    \( scores \gets [\ ] \) \;

    \PourCh{\( q \in \mathcal{Q} \setminus base \)\label{line-eval-predicates}}{
      \( c \gets base \wedge q \) \;
      \( Q^{\prime} \gets Q \vee c \) \;
      \( score_{Q^{\prime}} \gets fmeasure(Q^{\prime}, S^*) \) \;

      \( clauses \gets \) ajouter \( c \) à \( clauses \) \;
      \( scores \gets \) ajouter \( score_{Q^{\prime}} \) à \( scores \) \;

      \Si{\( score_{Q^{\prime}} > score^* \)}{
        \( score^* \gets score_{Q^{\prime}} \) \;
        \( clause^* \gets c \) \;
      }
    }
    \( clauses \gets \) trier \( clauses \) selon \( scores \) dans l'ordre décroissant \;

    \tcc*[l]{Parcours des \( faisceau \) meilleures clauses}
    \PourCh{\( c \in clauses[1 \ldots faisceau] \)}{
      \( faisceau\_c \gets MeilleurClause(E, S^*, \mathcal{Q}, Q, c, faisceau) \)\label{line-recursive-call} \;
      \( Q' \gets Q \vee faisceau\_c \) \;

      \( score \gets fmeasure(Q^{\prime}, S^*) \) \;

      \Si{\( score > score^* \)}{
        \( score^* \gets score \) \;
        \( clause^* \gets c \) \;
      }
    }

    \Retour{\( clause^* \)} \;
  }

  \caption{Recherche de la \emph{meilleure} clause conjonctive.}\label{algo-findclause}
\end{algorithm}

L'algorithme LPS Glouton a déjà été présenté dans les grandes lignes en \cref{algo-greedy}. Seule la stratégie de recherche n'a pas été présentée. Elle est présentée en \cref{algo-findclause} et est la pierre angulaire de LPS Glouton. C'est en effet au cours de l'exécution de cette stratégie que sont effectués la plupart des calculs, comme le calcul des fermés élémentaires des espaces prétopologiques candidats.

Cette stratégie permet de rechercher la clause conjonctive \( clause^* \) maximisant la F-mesure de la formule logique \( Q \vee clause^* \), \( Q \) étant donnée en entrée de la fonction. La recherche s'effectue par spécialisation de la clause conjonctive vide, et ce tant que la clause la plus spécifique n'est pas atteinte (\cref{line-end-rec}). \Cref{algo-findclause} est appelée la première fois en \cref{line-greedy-find-best} de \Cref{algo-greedy}. La première phase de la fonction consiste à évaluer toutes les clauses conjonctives plus spécialisées et composées d'un prédicat de plus que la clause \( base \) fournie en entrée, c'est-à-dire toutes les clauses formées par \( base \) et un prédicat de \( \mathcal{Q} \) qui n'est pas déjà dans \( base \) (\Cref{line-eval-predicates}). Les clauses candidates ainsi évaluées sont ensuite triées par ordre décroissant de F-mesure et seules les \( faisceau \) meilleures sont conservées. La seconde boucle de la fonction consiste à appeler la fonction récursivement en faisant varier le paramètre \( base \) avec les valeurs des clauses sélectionnées précédemment (\cref{line-recursive-call}). Ces appels récursifs permettent d'évaluer des clauses de tailles supérieures.

La complexité, en temps, de LPS Glouton est de l'ordre de \( O(maxiter \cdot \left\lvert \mathcal{Q} \right\rvert \cdot faisceau) \), ce qui, en pratique, est bien inférieur à celle de LPS Génétique puisque \( \left\lvert \mathcal{Q} \right\rvert \cdot faisceau \) est généralement substantiellement inférieur à la taille de la population initiale de LPS Génétique.

\medskip

Telle que définie dans cette section, la méthode LPS Glouton est bien plus simple à paramétrer que LPS Génétique puisque le seul meta-paramètre est la taille du faisceau de recherche. En outre, sa complexité en temps est bien moindre. Certaines expérimentations ont même révélé que LPS Glouton est capable de fournir de bien meilleurs résultats que LPS Génétique, et ce, malgré le fait que la stratégie de recherche en faisceau soit sous-optimale. Ces résultats sont très certainement dus à un paramétrage inadapté, puisque difficile, de l'algorithme génétique sous-jacent.

LPS Glouton est pensé de sorte à apprendre tout type d'espaces prétopologiques. Or, on a pu voir en \cref{chap-etat-art} qu'il est plus courant de manipuler des espaces prétopologiques de type V. Ces espaces possèdent des propriétés structurelles fortes et intéressantes. Si le type de l'espace prétopologique à apprendre est connu pour être de type V, comme c'est le cas dans \textcite{DBLP-conf/pkdd/CleuziouD15}, il serait regrettable de ne pas exploiter ces caractéristiques afin de guider l'algorithme d'apprentissage vers une solution plus satisfaisante. Le chapitre suivant est consacré spécifiquement à ce sujet.