# $texengine = "pdflatex"
$texengine = "lualatex"

& $texengine --aux-directory=auxs --output-directory=output --enable-write18 --interaction=errorstopmode 000-main.tex
biber --input-directory=auxs 000-main
& $texengine --aux-directory=auxs --output-directory=output --enable-write18 --interaction=errorstopmode 000-main.tex
& $texengine --aux-directory=auxs --output-directory=output --enable-write18 --interaction=errorstopmode 000-main.tex

# latexmk.exe --lualatex --aux-directory=auxs --output-directory=output --output-format=pdf --enable-write18 --interaction=errorstopmode 000-main