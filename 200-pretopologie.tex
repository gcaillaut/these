\chapter{Prétopologie}\label{chap-pretopologie}

La théorie de la prétopologie a vu le jour au début des années 1970~\autocite{belmandt1993manuel}. L'objectif des chercheurs ayant contribué à sa création était de construire une théorie plus souple que la topologie. En effet, il apparaissait comme une évidence que certains phénomènes réels ne pouvait convenablement être décrits par les outils de l'époque. C'est ce constat qui a motivé les travaux fondateurs de la prétopologie.

La prétopologie propose notamment d'étudier le concept de \emph{proximité}. Il est relativement simple, pour nous, d'interpréter et de comprendre cette notion dans un contexte donné. Il est toutefois plus ardu de définir formellement le raisonnement qui nous a poussé à interpréter le fait que deux objets soient proches, ou similaires. Si l'on considère deux formes géométriques colorées, sont-elles semblables car elles partagent la même couleur ? Le même nombre de côtés ? La même aire ? A priori ces trois critères semblent pertinents et doivent être pris en considération. Se pose alors la question de l'importance de chacun de ces critères : un petit triangle bleu est-il plus proche d'un petit carré bleu ou d'un gros triangle rouge ? La réponse dépendra probablement de l'objectif visé. C'est en ce sens que le contexte dans lequel on se place est important.

Cet exemple montre que derrière son apparente simplicité, la notion de \emph{proximité} n'est en réalité pas si évidente à définir. La prétopologie apporte un cadre théorique formel dans lequel décrire ces relations de proximité entre objets.

\medskip

Ce chapitre a pour objectif de décrire la théorie de la prétopologie dans sa globalité. Dans cette optique, une première section permettra de définir formellement la notion d'\emph{espace prétopologique} sur laquelle repose la théorie de la prétopologie. La seconde section consistera en une présentation d'espaces prétopologiques \emph{remarquables}. Une comparaison avec des théories analogues sera faite dans la troisième section.

\section{Espaces prétopologiques}

On considère un ensemble \( E \) d'éléments sur lequel on distingue un processus d'érosion ainsi qu'un processus de dilatation (voir \cref{fig-interieur,fig-adherence}). Ces deux processus sont décrits par, respectivement, les fonctions \( i : \PE \rightarrow \PE \) et \( a : \PE \rightarrow \PE \). Le triplet \( (E, i, a) \) est alors un \emph{espace prétopologique} défini par la population \( E \), l'opérateur d'\emph{intérieur} \( i(.) \) et l'opérateur d'\emph{adhérence} \( a(.) \).

\begin{definition}[C-dualité]\label{def-c-dual}
  On considère un ensemble \( E \) d'éléments. Soit \( i(.) \) et \( a(.) \) deux fonctions de \( \PE \) vers \( \PE \).
  On note \( c(.) \) l'opérateur complémentaire défini pour tout ensemble \( A \) de \( \PE \) par \( c(A) = E \setminus A \).

  \smallskip
  \noindent
  Les fonctions \( i(.) \) et \( a(.) \) sont dites \emph{c-duales} si et seulement si :
  \begin{enumerate}[label=\roman*)]
    \item \( i = c \circ a \circ c \) ;
    \item \( a = c \circ i \circ c \).
  \end{enumerate}
\end{definition}

\begin{definition}[Intérieur et adhérence]\label{def-interieur-adherence}
  On considère un ensemble \( E \) d'éléments. Soit \( i(.) \) et \( a(.) \) deux fonctions c-duales de \( \PE \) vers \( \PE \).
  
  \smallskip
  \noindent
  Alors \( i(.) \) et \( a(.) \) sont, respectivement, des fonctions d'intérieur et d'adhérence si et seulement si :
  \begin{enumerate}[label=\roman*)]
    \item \( a(.) \) et \( i(.) \) sont c-duales ;
    \item\label{item-int1} \( \forall A \in \PE,\, i(A) \subseteq A \) ;
    \item\label{item-adh1} \( \forall A \in \PE,\, A \subseteq a(A) \) ;
    \item\label{item-int2} \( i(E) = E \) ;
    \item\label{item-adh2} \( a(\emptyset) = \emptyset \).
  \end{enumerate}
\end{definition}

Puisque \( i(.) \) est définie en fonction de \( a(.) \), et inversement \( a(.) \) est définie en fonction de \( i(.) \), on peut simplifier la notation de l'espace prétopologique \( (E, i, a) \) par \( (E, i) \) ou \( (E, a) \). En pratique, c'est la définition par l'adhérence qui sera utilisée dans ce document.

Du fait que les opérateurs \( i(.) \) et \( a(.) \) sont c-duales, \emph{\ref{item-int1}} et \emph{\ref{item-adh1}} sont en réalité équivalents. Il en est de même pour \emph{\ref{item-int2}} et \emph{\ref{item-adh2}}.

\begin{proof}
  Soit \( (E, i, a) \) un espace prétopologique. Démontrons dans un premier temps l'équivalence entre \emph{\ref{item-int1}} et \emph{\ref{item-adh1}}.

  L'opérateur d'intérieur est défini tel que \( \forall A \in \PE,\, i(A) \subseteq A \), on a donc naturellement \( \forall A \in \PE,\, i(c(A)) \subseteq c(A) \).
  \begin{align*}
    \forall A \in \PE,\, i(A) \subseteq A &\Rightarrow i(c(A)) \subseteq c(A) \\
    &\Rightarrow c(i(c(A))) \supseteq c(c(A)) \\
    &\Rightarrow a(A) \supseteq A
  \end{align*}
  Puisque, par définition, \( c(i(c(A))) \) équivaut à \( a(A) \). On obtient donc \( A \subseteq a(A) \) à partir de \( i(A) \subseteq A \). On peut aisément démontrer l'implication inverse de la même manière.

  \smallskip

  À présent, démontrons que \emph{\ref{item-int2}} et \emph{\ref{item-adh2}} sont équivalents. Il est clair que \( i(\emptyset) = \emptyset \) et \( a(E) = E \), puisqu'on ne peut ni réduire l'ensemble vide, ni augmenter \( E \).
  \begin{align*}
    a(E) &= E & \\
    a(c(E)) &= \emptyset & \text{car } c(E) = \emptyset \text{ et } a(\emptyset) = \emptyset \text{ par définition} \\
    c(a(c(E))) &= E & \\
    i(E) &= E & \text{puisque } i = c \circ a \circ c
  \end{align*}
  On peut prouver de la même manière que \( a(\emptyset) = \emptyset \) à partir de \( i(\emptyset) = \emptyset \).
\end{proof}

\begin{figure}
  \begin{minipage}{0.45\linewidth}
    \centering
    \includefigure[width=\linewidth]{interieur}
    \caption{L'opérateur d'intérieur permet de réduire un ensemble vers un autre, plus petit (au sens de l'inclusion).}\label{fig-interieur}
  \end{minipage}\hfill
  \begin{minipage}{0.45\linewidth}
    \centering
    \includefigure[width=\linewidth]{adherence}
    \caption{L'opérateur d'adhérence permet d'étendre un ensemble vers un autre, plus grand (au sens de l'inclusion).}\label{fig-adherence}
  \end{minipage}
\end{figure}

La théorie de la prétopologie est bâtie sur ces deux opérateurs fondamentaux. Par exemple, les opérateurs d'\emph{extérieur}, de \emph{frontière}, de \emph{bord} et \emph{d'orle}\footnote{Certains auteurs parlent d'opérateur d'\emph{abord}.} sont définis par les fonctions d'intérieur et d'adhérence~\autocite{thibault-halshs-01583811}. Ces opérateurs sont définis pour tout élément de \( \PE \) et retournent également un élément \( \PE \).

L'extérieur \( e(A) \) d'un ensemble \( A \) représente ce qui est \emph{éloigné} de \( A \). C'est ce qui n'est pas atteignable \emph{directement} par \( A \). La frontière \( f(A) \) de \( A \) représente les éléments \emph{entre} \( A \) et son extérieur. Elle est composée de deux parties, le bord \( b(A) \) et l'orle \( o(A) \) représentant ce qu'on peut décrire comme les frontières \emph{interne} et \emph{externe} de \( A \). Les définitions de ces opérateurs sont données ci-dessous et leurs comportements sont illustrés en \cref{fig-exterieur-bord-orle-frontiere}.
\begin{align*}
  \forall A \in \PE,\, && e(A) &= c(a(A)) \\
  \forall A \in \PE,\, && b(A) &= A \setminus i(A) \\
  \forall A \in \PE,\, && o(A) &= a(A) \setminus A \\
  \forall A \in \PE,\, && f(A) &= b(A) \cup o(A)
\end{align*}

\begin{figure}
  \centering
  \includefigure{exterieur-bord-orle-frontiere}
  \caption{Les opérateurs d'extérieur, de bord, d'orle et de frontière.}\label{fig-exterieur-bord-orle-frontiere}
\end{figure}

Les opérateurs d'intérieur et d'adhérence sont soumis à un nombre très limité de contraintes, offrant ainsi plus de souplesse et d'expressivité que ce qui est permis par la topologie. Par exemple, les opérateurs d'intérieur et d'adhérence ne sont pas nécessairement contraints de respecter la propriété d'idempotence. On note alors \( i^n(.) \) et \( a^n(.) \) les fonctions itérées consistant à appliquer \( n \) fois les opérateurs d'intérieur et d'adhérence.

\begin{align*}
  \forall A \in \mathcal{P}(E),\, \forall k \in \NatNums,\, i^0(A) &= A \text{ et } i^k(A) = (i \circ i^{k-1})(A) \\
  \forall A \in \mathcal{P}(E),\, \forall k \in \NatNums,\, a^0(A) &= A \text{ et } a^k(A) = (a \circ a^{k-1})(A)
\end{align*}

Les opérateurs d'intérieur et d'adhérence peuvent alors être utilisés pour décrire les différentes étapes d'un processus d'érosion ou de dilatation. Un tel processus commence dans un état initial donné, les étapes suivantes découlent l'une de l'autre pour aboutir à un état terminal stable, comme illustré en \cref{fig-ouverture-fermeture}. En d'autres termes, les opérateurs d'intérieur et d'adhérence peuvent être appliqués de façon successive jusqu'à obtention d'un point fixe. Ce point fixe est qualifié d'\emph{ouvert} s'il est obtenu par l'opérateur d'intérieur ou de \emph{fermé} s'il est obtenu par l'opérateur d'adhérence. Plus généralement, un ouvert (respectivement un fermé) est un sous-ensemble de \( E \) tel que son intérieur (respectivement son adhérence) est identique à lui-même.

\begin{definition}[Ensembles ouvert/fermé]\label{def-ouvert-ferme}
  Soit \( (E, i, a) \) un espace prétopologique et \( K \in \PE \) une partie de \( E \).
  \begin{itemize}
    \item \( K \) est un ouvert de \( (E, i, a) \) si est seulement si \( K = i(K) \)
    \item \( K \) est un fermé de \( (E, i, a) \) si est seulement si \( K = a(K) \)
  \end{itemize}
\end{definition}

\begin{definition}[Opérateurs d'ouverture et de fermeture]\label{def-ouverture-fermeture}
  Soit \( (E, i, a) \) un espace prétopologique, \( A \in \PE \) une partie de \( E \) et \( k \) un entier positif.

  \smallskip
  \noindent
  On appelle opérateur d'ouverture une fonction \( O : \PE \rightarrow \PE \) définie par
  \begin{equation*}
    \forall A \in \PE,\, O(A) = i^k(A) \text{ avec } i^k(A) = i^{k + 1}(A)
  \end{equation*}
  
  \smallskip
  \noindent
  On appelle opérateur de fermeture une fonction \( F : \PE \rightarrow \PE \) définie par
  \begin{equation*}
    \forall A \in \PE,\, F(A) = a^k(A) \text{ avec } a^k(A) = a^{k + 1}(A)
  \end{equation*}
\end{definition}

\begin{figure}
  \centering
  \includefigure[width=\linewidth]{ouverture-fermeture}
  \caption{Les opérateurs d'ouverture et de fermeture décrivent respectivement des processus de rétractation et d'expansion.}\label{fig-ouverture-fermeture}
\end{figure}

\'{E}tant donné un ensemble \( A \) de \( \PE \), on appelle ouverture de \( A \) le plus grand ensemble ouvert inclus dans \( A \). L'ouverture de \( A \) correspond à l'union des ouverts inclus dans \( A \), si cette union est un ouvert, alors elle correspond à l'ouverture de \( A \), sinon l'ouverture de \( A \) est indéfinie. \`{A} l'inverse, on appelle fermeture de \( A \) le plus petit fermé incluant \( A \). Elle correspond à l'intersection entre les fermés incluant \( A \). Si l'intersection est un fermé, alors elle correspond à la fermeture de \( A \), sinon sa fermeture est indéfinie.

Il faut prendre garde à distinguer l'ouvert de \( A \) de l'ensemble \( O(A) \) résultant de l'application de l'opérateur d'ouverture, de même pour le fermé de \( A \) et \( F(A) \). En effet, l'existence de l'ouverture ou de la fermeture de \( A \) n'est pas garantie, comme le montre l'exemple tiré de \textcite{belmandt1993manuel} en \cref{tab-ouverts-fermes-non-definis}. Les opérateurs \( O(.) \) et \( F(.) \) sont quant à eux définis pour tout ensemble de \( \PE \).

\begin{table}
  \centering
  \includetable{ouverts-fermes-non-definis}
  \caption{La fermeture du singleton \( \{ x \} \) est indéfinie puisque l'intersection entre les fermés incluant \( \{ x \} \) (\( \{ x, y \} \), \( \{ x, z \} \) et \( \{ x, y, z \} \)) n'est pas un fermé. L'ouverture de \( \{ y, z \} \) est indéfinie car l'union entre les ouverts inclus dans \( \{ y, z \} \) (\( \emptyset \), \( \{ y \} \) et \( \{ z \} \)) n'est pas un ouvert. Pourtant \( F( \{ x \} ) \) et \( O( \{ y, z \} ) \) sont bels et bien définis et valent respectivement \( \{ x, z \} \) et \( \{ y \} \).}\label{tab-ouverts-fermes-non-definis}
\end{table}

Il existe toutefois des espaces prétopologiques dans lesquels ces deux notions sont confondues. Ces espaces appartiennent à la famille des \emph{espaces prétopologiques de type V} et sont le sujet d'une prochaine section (\cref{sec-pretopologie-type-v}).

\section{Comparaisons d'espaces prétopologiques}

Soit \( E \) un ensemble d'éléments. On désigne par \( \mathcal{T}(E) \) l'ensemble des espaces prétopologiques définis sur \( E \). Il existe une relation d'ordre partiel sur les éléments de \( \mathcal{T}(E) \). On note cette relation \( < \).

\begin{equation*}
  \begin{split}
    \forall (E, i, a) \in \mathcal{T}(E),\, \forall (E, i^{\prime}, a{\prime}) \in \mathcal{T}(E),\, \forall A \in \PE,\, \\
    (E, i, a) < (E, i^{\prime}, a^{\prime}) &\Leftrightarrow a(A) \subset a^{\prime}(A) \\
    (E, i, a) < (E, i^{\prime}, a^{\prime}) &\Leftrightarrow i(A) \supset i^{\prime}(A)
  \end{split}
\end{equation*}

\begin{figure}
  \centering
  \includefigure{comparaison-pretopo}
  \caption{\`{A} gauche, l'espace prétopologique \( (E, a) \) est plus fin que \( (E, a^{\prime}) \). \`{A} droite, les deux espaces prétopologiques ne sont pas comparables.}\label{fig-comparaison-pretopo}
\end{figure}

Soit \( (E, i, A) \) et \( (E, i^{\prime}, a^{\prime}) \) deux espaces prétopologiques. On dit que \( (E, i, A) \) est \emph{plus fin} que \( (E, i^{\prime}, a^{\prime}) \) si et seulement si \( a(A) \) est inclus dans \( a^{\prime}(A) \) pour toute partie \( A \) de \( E \), ce qui est équivalent à \( i^{\prime}(A) \) est inclus dans \( i(A) \).

On peut interpréter la relation de finesse entre deux espaces prétopologiques comme étant la << vitesse >> de propagation des parties de \( E \), par leurs opérateurs d'adhérence, vers leurs fermetures. Un espace fin aura tendance à étendre ses ensembles lentement, nécessitant ainsi de nombreuses étapes avant d'atteindre un ensemble fermé.

\medskip

Il existe un espace prétopologique plus fin que tous les autres espaces de \( \mathcal{T}(E) \). Cet espace prétopologique est tel que \( a(A) \) est égal à \( A \), pour toutes parties \( A \) de \( E \). La prétopologie ainsi définie est qualifiée de \emph{discrète}.

Il existe aussi un espace prétopologique moins fin que tous les autres espaces de \( \mathcal{T}(x) \). Il est tel que \( a(E) \) soit égale à \( E \) pour toute partie \( A \) de \( E \). On qualifie une telle prétopologie de \emph{grossière}.


\section{Espaces prétopologiques de type V}\label{sec-pretopologie-type-v}

Un espace prétopologique \( (E, i, a) \) appartient à la famille des espaces prétopologiques de type V si et seulement si \( i(.) \) et \( a(.) \) sont des \emph{fonctions isotones}. On parle aussi de fonctions \emph{Schur-convexes} ou de fonctions \emph{préservant l'ordre}.

\begin{definition}[Espace prétopologique de type V]
  Un espace prétopologique \( (E, i, a) \) est de type V si et seulement si ses fonctions d'intérieur et d'adhérence sont isotones.

  \begin{align*}
    \forall A \in \PE,\, \forall B \in \PE,\, A \subseteq B &\Rightarrow i(A) \subseteq i(B) \\
    \forall A \in \PE,\, \forall B \in \PE,\, A \subseteq B &\Rightarrow a(A) \subseteq a(B)
  \end{align*}

  En réalité, il suffit de montrer qu'un des deux opérateurs respecte l'isotonie pour que le second la respecte aussi.
\end{definition}

Les espaces prétopologiques de type V possèdent des propriétés structurelles intéressantes. On peut notamment montrer que l'intersection de fermés donne un fermé et l'union d'ouverts un ouvert.

\begin{proof}
  Soit \( (E, i, a) \) un espace prétopologique de type V. Montrons que toute intersection de fermés donne un fermé.

  Soit \( A \) et \( B \) deux fermés de \( (E, i, a) \) et \( C = A \cap B \) l'intersection entre \( A \) et \( B \).
  On a alors \( C \subseteq A \) et \( C \subseteq B \), et donc, par définition d'un espace prétopologique de type V, \( a(C) \subseteq a(A) \) et \( a(C) \subseteq a(B) \).

  Soit un élément \( x \in E \), si \( x \in a(C) \) alors \( c \in a(A) \) et \( x \in a(B) \). Or, \( A \) et \( B \) sont fermés, donc \( A = a(A) \) et \( B = a(B) \). Par conséquent, \( x \in A \) et \( x \in B \). \( C \) contient tous les éléments présents à la fois dans \( A \) et \( B \), donc \( x \in C \).

  \begin{align*}
    \forall x \in E,\, x \in a(C) &\Rightarrow x \in a(A) \wedge x \in a(B) & \text{par isotonie}\\
    &\Leftrightarrow x \in A \wedge x \in B & \text{car } A \text{ et } B \text{ sont des fermés}\\
    &\Leftrightarrow x \in A \cap B & \\
    &\Leftrightarrow x \in C & \text{puisque } C = A \cap B
  \end{align*}

  Tout élément de l'adhérence de \( C \) appartient à \( C \), \( C \) est donc un fermé. On peut généraliser cette démonstration à des intersections de plusieurs fermés en remarquant astucieusement que \( A \cap B \cap C = ( A \cap B ) \cap C \).

  On peut démontrer de façon analogue que toute union d'ouverts donne un ouvert.
\end{proof}

D'autre part, tout ensemble \( A \) de \( \PE \) admet un plus grand ouvert et un plus petit fermé, correspondant respectivement à \( O(A) \) et \( F(A) \).

\begin{property}\label{prop-ferme-minimal}
  Soit \( (E, i, a) \) un espace prétopologique de type V. Toute partie \( A \) de \( E \) admet un plus grand ouvert et un plus petit fermé, correspondant respectivement à \( O(A) \) et \( F(A) \).
\end{property}

\begin{proof}
  Soit \( (E, i, a) \) un espace prétopologique de type V. Montrons que toute partie \( A \) de \( E \) admet un plus petit fermé et qu'il correspond à \( F(A) \).

  Il est évident que \( F(A) \) est un fermé incluant \( A \). Montrons que c'est le plus petit, c'est-à-dire qu'il correspond à l'intersection des fermés incluant \( A \).

  Soit \( K \) un fermé tel que \( A \subseteq K \). Par isotonie, on a \( F(A) \subseteq F(K) \). Or \( K \) est un fermé, on a donc \( K = F(K) \) et donc \( F(A) \subseteq K \).

  \begin{align*}
    \forall K \in \PE,\, K = a(K),\, A \subseteq K &\Rightarrow F(A) \subseteq F(K) & \text{par isotonie} \\
    &\Rightarrow F(A) \subseteq K & \text{car } K \text{ est un fermé} \\
    &\Rightarrow F(A) \cap K = F(A) & \\
  \end{align*}

  Tout ensemble \( K \) fermé incluant \( A \) inclue aussi \( F(A) \). \( F(A) \) étant lui-même un fermé incluant \( A \), l'intersection des fermés incluant \( A \) est \( F(A) \). \( F(A) \) est donc le plus petit fermé incluant \( A \).

  On peut démontrer de façon analogue que toute partie \( A \) de \( E \) admet un plus grand ouvert correspondant à \( O(A) \).
\end{proof}

\subsection{Définition par les préfiltres}

Les espaces prétopologiques de type V sont indissociables de la notion de \emph{préfiltre}.

\begin{definition}{Préfiltre}
  Un préfiltre \( \mathcal{F} \) sur \( E \) est une famille de parties de \( E \) stable par passage à tout sur-ensemble~\autocite{belmandt1993manuel}.

  \begin{equation*}
    \forall F \in \mathcal{F},\, \exists H \in \PE,\, F \subseteq H \Rightarrow H \in \mathcal{F}
  \end{equation*}
\end{definition}

Tout espace prétopologique de type V est défini par une famille de préfiltres sur \( E \). Soit un espace prétopologique \( (E, i, a) \) de type V, on peut associer à tout élément \( x \) de \( E \) une famille \( \mathcal{V}(x) \) de parties de \( E \).

\begin{equation*}
  \forall x \in E,\, \mathcal{V}(x) = \{ V \in \PE \mid x \in i(V) \}
\end{equation*}

La famille \( \mathcal{V}(x) \) est un préfiltre sur \( E \) dont tous les éléments contiennent \( x \), puisque \( x \) appartient à leurs intérieurs et que \( (E, i, a) \) est de type V. \( \mathcal{V}(x) \) est appelé \emph{voisinages de \( x \)}.

On peut définir l'opérateur d'adhérence (et donc l'opérateur c-dual d'intérieur) par les voisinages \( \mathcal{V}(x) \) d'un point \( x \).

\begin{equation*}
  \forall A \in \PE,\, a(A) = \{ x \in E \mid \forall V \in \mathcal{V}(x),\, V \cap A \neq \emptyset \}
\end{equation*}

En pratique, il est inutile de manipuler des préfiltres complets. En effet, l'adhérence d'une partie \( A \) de \( E \) contient les éléments de \( E \) tels que tous leurs voisinages possèdent une intersection non nulle avec \( A \). Or, dès lors que les << plus petits >> éléments de \( \mathcal{V}(x) \) intersectent \( A \) en un point, les << plus grand >> intersectent \( A \) en ce même point. Il est donc plus commode de ne considérer que les plus petits éléments de \( \mathcal{V}(x) \).

\begin{definition}[Base de préfiltre]
  Soit \( E \) un ensemble et \( \mathcal{M} \) une famille de \( \PE \). \( \mathcal{M} \) engendre un unique préfiltre \( \mathcal{F} \) sur \( E \) tel que \( \mathcal{F} \) contient les sur-ensembles des éléments de \( \mathcal{M} \).

  \begin{equation*}
    \mathcal{F} = \{ A \in \PE \mid \exists M \in \mathcal{M},\, M \subseteq A \}
  \end{equation*}

  \noindent
  On dit que \( \mathcal{M} \) est une base de \( \mathcal{F} \) ou encore que \( \mathcal{M} \) engendre \( \mathcal{F} \).
\end{definition}

On note \( \mathcal{B}(x) \) la fonction associant a tout élément \( x \) de \( E \) une base de voisinages telle que \( \mathcal{B}(x) \) est une base du préfiltre \( \mathcal{V}(x) \). On peut alors définir des fonctions d'intérieur et d'adhérence équivalentes à celles définies par \( \mathcal{V} \).

\begin{align}\label{eqn-adherence-base-prefiltre}
  \forall A \in \PE,\, i(A) &= \{ x \in E \mid \exists V \in \mathcal{B}(x),\, V \subseteq A \} \\
  \forall A \in \PE,\, a(A) &= \{ x \in E \mid \forall V \in \mathcal{B}(x),\, V \cap A \neq \emptyset \}
\end{align}

\begin{figure}
  \centering
  \includefigure{prefiltre}
  \caption{En jaune, le préfiltre \( \mathcal{V}(x_2) \) et en vert une base de \( \mathcal{V}(x_2) \). Dans cet exemple, la base est minimale.}\label{fig-prefiltre}
\end{figure}

\subsection{Catégories d'espaces prétopologiques de type V}

Il existe différentes catégories d'espaces prétopologiques de type V. On distingue notamment les espaces prétopologiques de type \Vd{}, les espaces prétopologiques de type \Vs{} et les espaces topologiques.

\begin{definition}[Espace prétopologique de type \Vd]
  Un espace prétopologique \( (E, i, a) \) est de type \Vd{} si et seulement si pour toutes parties \( A \) et \( B \) de \( E \) l'adhérence de l'union entre \( A \) et \( B \) est équivalente a l'union des adhérences de ces mêmes ensembles.

  \begin{equation*}
    \forall A \in \PE,\, \forall B \in \PE,\, a(A \cup B) = a(A) \cup a(B)
  \end{equation*}

  \noindent
  Un espace prétopologique de type \Vd{} est nécessairement de type V.
\end{definition}

\begin{definition}[Espace prétopologique de type \Vs{}]
  Un espace prétopologique \( (E, i, a) \) est de type \Vs{} si et seulement si pour toute partie \( A \) de \( E \), l'adhérence de \( A \) est équivalente à l'union des adhérences des singletons inclus dans \( A \).

  \begin{equation*}
    \forall A \in \PE,\, a(A) = \bigcup_{x \in A} a(\{ x \})
  \end{equation*}

  \noindent
  Un espace prétopologique de type \Vs{} est nécessairement de type \Vd{}.
\end{definition}

\begin{definition}[Espace topologique]
  Un espace prétopologique \( (E, i, a) \) est un espace topologique si et seulement s'il est de type \Vd{} et que sa fonction d'adhérence est idempotente.

  \begin{equation*}
    \forall A \in \PE,\, a(A) = a(a(A))
  \end{equation*}
\end{definition}

Ainsi, les différentes classes d'espaces prétopologiques sont organisées au sein d'une structure hiérarchique, comme montré en \cref{fig-hierarchie-pretopologie}. Un espace prétopologique dont le type serait haut placé dans la hiérarchie permettrait de résoudre et de modéliser des problèmes plus complexes et variés. En contrepartie, il sera plus difficile d'exploiter et d'interpréter un tel modèle. Les espaces prétopologiques de type V semblent offrir un bon compromis entre expressivité et simplicité, les travaux scientifiques autour de la prétopologie sont en effet principalement centrés sur ce type d'espace prétopologique.

\begin{figure}
  \centering
  \includefigure{hierarchie-pretopologie}
  \caption{Schéma d'inclusion des différentes familles d'espaces prétopologiques.}\label{fig-hierarchie-pretopologie}
\end{figure}

\subsection{Construction d'espaces prétopologiques de type V}\label{subsec-construction-type-v}

Tout espace prétopologique \( (E, i, a) \) de type V se défini par une famille de voisinages, ou préfiltres, sur \( E \). Or, il est plutôt rare de définir un tel espace de cette façon. Il est plus courant de définir un espace prétopologique par un ensemble de relations binaires sur \( E \). On note \( \mathcal{R} \) l'ensemble des relations, et \( x R_i y \) désigne le fait qu'un élément \( x \) de \( E \) soit en relation selon un élément \( R_i \) de \( \mathcal{R} \) avec un autre élément \( y \) de \( E \).

Pour toute relation \( R_i \) de \( \mathcal{R} \), on définit l'ensemble \( \Gamma_i(x) \) des descendants d'un élément \( x \) de \( E \).

\begin{equation*}
  \forall R_i \in \mathcal{R},\, \forall x \in E,\, \Gamma_i(x) = \{ y \in E \mid x{R}_{i}y \}
\end{equation*}

On peut alors définir un opérateur d'adhérence à partir des familles des descendants.

\begin{equation*}
  \forall A \in \PE,\, a(A) = \{ x \in E \mid \forall R_i \in \mathcal{R},\, \Gamma_i(x) \cap A \neq \emptyset \}
\end{equation*}

Cet opérateur d'adhérence ressemble à s'y méprendre à l'opérateur d'adhérence défini en \cref{eqn-adherence-base-prefiltre}. En effet, pour tout élément \( x \) de \( E \), on peut construire une base \( \mathcal{B}(x) \) de préfiltre définie par \( \mathcal{R} \).

\begin{equation*}
  \forall x \in E,\, \mathcal{B}(x) = {\{ {\Gamma}_{i}(x) \}}_{R_i \in \mathcal{R}}
\end{equation*}

On peut alors reformuler la fonction d'adhérence pour l'exprimer en fonction de \( \mathcal{B} \) et ainsi retrouver la formule en \cref{eqn-adherence-base-prefiltre}.

\begin{equation*}
  \forall A \in \PE,\, a(A) = \{ x \in E \mid \forall B \in \mathcal{B}(x),\, B \cap A \neq \emptyset \}
\end{equation*}

Toutefois, cette construction s'avère être plus restrictive. En effet, en définissant \( \mathcal{B} \) par \( \mathcal{R} \), on impose au cardinal de toutes les bases \( \mathcal{B}(x) \) d'être égal au nombre de relations dans \( \mathcal{R} \). Ce qui, pour un nombre de relations données, réduit le pouvoir expressif d'une telle construction. La définition par les préfiltres n'impose aucune restriction de la sorte, chaque élément peut avoir une base de voisinages de taille quelconque.

Enfin, il est important de signaler que si \( \mathcal{R} \) ne comporte qu'une seule relation, l'espace prétopologique en découlant est de type \Vs{}.

\medskip

\textcite{bul.6662319830101} propose plusieurs façons de construire un espace prétopologique à partir d'un ensemble \( E \) muni d'une fonction \( d \) de dissimilarité. La prétopologie des \( k \) plus proches voisins est définie selon un entier \( k \) positif et la relation \( V_k \) de voisinages associant à tout élément \( x \) de \( E \) ses \( k \) plus proches voisins.

\begin{equation*}
  \forall x \in E,\, V_k(x) = \{ y \in E \mid y \text{ est un des } k \text{ plus proches voisins de } x \}
\end{equation*}

L'espace prétopologique \( (E, a_k) \) des \( k \) plus proches voisins peut alors être défini par les bases de voisinages \( \mathcal{B}(x) = \{ V_k(x) \} \). On peut ainsi définir des espaces prétopologiques plus ou moins fins en faisant varier \( k \) entre \( 0 \) et \( \lvert E \rvert - 1 \). Dans le cas où \( k \) est minimal, l'espace prétopologique est équivalent à la prétopologie discrète. \`{A} l'inverse, si \( k \) est maximal alors l'espace prétopologique correspond à la prétopologie grossière.

Dans la même lignée, \citeauthor{bul.6662319830101} propose la notion de \( \epsilon \)-voisinages où \( \epsilon \) est un nombre réel positif. La prétopologie des \( \epsilon \)-voisins est alors définie par la relation \( V_{\epsilon} \) de voisinages et \( \epsilon \) désigne un seuil en deçà duquel un élément de \( E \) est voisin à un autre.

\begin{equation*}
  \forall x \in E,\, V_{\epsilon}(x) = \{ y \in E \mid d(x, y) \le \epsilon \}
\end{equation*}

On peut définir des espaces prétopologiques \( (E, a_{\epsilon}) \) plus ou moins fins en faisant varier \( \epsilon \) entre la plus petite et la plus grande valeur prise par \( d(x, y) \), c'est-à-dire entre le plus petit et le plus grand écart entre deux éléments de \( E \). Si \( \epsilon \) est égal (ou inférieur) au plus petit écart, alors l'espace prétopologique \( (E, a_{\epsilon}) \) correspond à la prétopologie discrète, tandis que si \( \epsilon \) est égal (ou supérieur) au plus grand écart, \( (E, a_{\epsilon}) \) désigne la prétopologie grossière.


\section{Théories connexes}

Comme énoncé précédemment, la théorie de la prétopologie étudie les relations entre les parties d'un ensemble \( E \) via les deux opérateurs \( c \)-duals d'intérieur et d'adhérence. Soit un espace prétopologique \( (E, i, a) \), on peut encadrer toute partie \( A \) de \( E \) par son intérieur \( i(A) \) et son adhérence \( a(A) \).

\begin{equation*}
  \forall A \in \PE,\, i(A) \subseteq A \subseteq a(A)
\end{equation*}

Dans un cadre purement prétopologique, on dira que \( i(A) \) désigne le sous-ensemble de \( A \) éloigné du reste de la population, c'est-à-dire du complémentaire de \( A \) ; tandis que \( a(A) \) désigne \( A \) augmenté des éléments de \( E \) proches de \( A \). On se rend vite compte que cette description est assez abstraite, pour cause, la prétopologie ne donne pas de définition universel de la notion de proximité. Au contraire, c'est l'application, donc la façon dont est construit l'espace prétopologique, qui donne un sens au concept abstrait de proximité.

\medskip

Ainsi, on peut chercher à décrire divers concepts au travers de cette notion de proximité. Notamment, on peut construire un espace prétopologique dans le but de formaliser la notion d'incertitude. Par exemple, étant donné une partie \( A \) de \( \PE \), l'intérieur \( i(A) \) capture le sous-ensemble << certain >> de \( A \), tandis que \( a(A) \) capture le sur-ensemble << probable >> de \( A \).

Cette interprétation particulière des opérateurs d'intérieur et d'adhérence est proche, voire équivalente, à la définition de l'incertitude dans la théorie des ensembles approximatifs ou \emph{rough sets} en anglais~\autocite{pawlak1982rough}. Dans cette théorie, on considère une population \( E \) et on adjoint à toute partie \( A \) de \( E \) une approximation basse \( \approxB(A) \) et une approximation haute \( \approxT(A) \).

\begin{equation*}
  \forall A \in \PE,\, \approxB(A) \subseteq A \subseteq \approxT(A) 
\end{equation*}

Tout ensemble approximatif consiste alors en une partie de \( E \) encadrée par deux autres parties de \( E \). Il est assez clair qu'on peut librement interchanger de \( \approxB(A) \) et \( i(A) \) ainsi que \( \approxT(A) \) et \( a(A) \) afin de passer d'une formalisation en théorie des ensembles approximatifs à une formalisation prétopologique.

% \medskip

% Quand on cherche à représenter et à quantifier l'incertitude, on ne peut ignorer la théorie des ensembles flous~\autocite{zadeh1965fuzzy}. Beaucoup de travaux étendent cette théorie, on peut par exemple citer la théorie des ensembles flous intuitionnistes~\autocite{ATANASSOV198687}, la théorie des ensembles flous hésitants~\autocite{torra2010hesitant} ou encore la théorie des possibilités~\autocites{zadeh1978fuzzy,Dubois2012}.

% Un ensemble flou \( A \) est défini sur une population \( E \) par sa \emph{fonction d'appartenance} \( \mu_A : E \rightarrow [0;1] \). De façon plus générale, et sans ajouter trop de complexité, la fonction d'appartenance associe un élément \( x \) de \( E \) à un élément d'un ensemble \( L \) partiellement ordonné~\autocite{goguen1967fuzzy}. On parle alors d'ensembles \( L \)-flous.

% \begin{equation*}
%   \mu_A : E \rightarrow L
% \end{equation*}

% On peut établir plusieurs liens entre les ensembles flous et la prétopologie. \textcite{bouayad1998pretopologie} propose de le démontrer en considérant deux sous-ensembles particuliers d'un ensemble \( A \) flou : son support et son noyau. Le graphique en \cref{fig-fuzzysets-noyau-support} met en évidence ces deux sous-ensembles.

% \begin{align*}
%   \support(A) &= \{ x \in E \mid \mu_A(x) \ge 0 \} \\
%   \noyau(A) &= \{ x \in E \mid \mu_A(x) = 1 \} \\
% \end{align*}

% \citeauthor{bouayad1998pretopologie} remarque que ces deux opérateurs, à l'instar des opérateur d'intérieur et d'adhérence, permettent d'encadrer n'importe quel ensemble \( A \) flou.

% \begin{equation*}
%   \noyau(A) \subseteq A \subseteq \support(A)
% \end{equation*}

% \begin{figure}
%   \centering
%   \includefigure{fuzzysets-noyau-support}
%   \caption{Fonction d'appartenance d'un ensemble flou.}\label{fig-fuzzysets-noyau-support}
% \end{figure}

% \'{E}tant donné un ensemble \( E \) d'élément, il propose alors de représenter les ensembles flous définis sur \( E \) dans un espace prétopologique \( (E, i, a) \) où les fonctions \( i(.) \) et \( a(.) \) sont respectivement équivalentes aux fonction \( \noyau(.) \) et \( \support(.) \).

% Ce pont entre la théorie de la prétopologie et la théorie des ensembles flous repose sur des fondations pour le moins instables. D'une part les fonctions de support et de noyau, telles que définies, sont excessivement réductrice. En général, pour un ensemble \( A \) flou, il n'y a pas de raison de penser qu'il éxiste un élément \( x \) de \( E \) pour lequel \( \mu_A(x) = 1 \). Par conséquent, le noyau d'un ensemble flou est souvent vide. Une quantité assez conséquente d'information est donc perdue dans la transition entre le modèle flou et le modèle prétopologique.

% Plus grave encore, les fonctions d'intérieur et d'adhérence d'un tel espace prétopologique sont définies sur l'ensembles \( \PE \) des parties de \( E \). Or, les fonctions de noyau et de support sont définies sur les ensembles flous définis sur \( E \), et non pas sur \( \PE \). En réalité, il faut considérer l'espace prétopologique \( (\mathcal{E}, i, a) \) où \( \mathcal{E} \) est l'ensemble des ensembles flous sur \( E \) et \( i(.) \) et \( a(.) \) sont, respectivement, les fonctions de noyau et de support.

% \medskip

% Il est en fait assez compliqué d'établir un lien direct entre prétopologie et ensembles flous, puisque la prétopologie permet d manipuler des données discrètes, l'ensemble \( \PE \) des parties de \( E \), contrairement aux ensembles flous qui sont par nature continues. On peut toutefois assimiler les niveaux d'adhérence et d'intérieur à des sortes de \( \alpha \)-coupes. Ainsi, toute partie \( A \) de \( E \) engendrerait un ensemble floue sur \( E \) caractérisé par \( \mu_A \) :

% \begin{equation*}
%   \forall A \in \PE,\, \forall x \in E ,\, \mu_A(x) =
%   \begin{cases}
%     1 & \text{si } x \in A \\
%     0 & \text{si } x \notin F(A) \\

%   \end{cases}
% \end{equation*}

% \medskip

% Il est plus commode d'établir un lien entre prétopologie est ensembles \( L \)-flous, lorsque \( L \) est discret. Pour simplifier les chose, on pose \( L = \Integers \). Dans ce contexte, un ensemble \( A \) \( L \)-flou est caractérisé par sa fonction d'appartenance \( \mu_A \) associant à tout élément de \( E \) un entier.

% \begin{equation*}
%   \mu_A : E \Rightarrow \Integers
% \end{equation*}

% Un valeur élevée de \( \mu_A(x) \) indiquerait que l'élément \( x \) est peu intégré, ou éloigné, de \( A \), tandis que des valeurs négatives indiqueraient l'inverse.