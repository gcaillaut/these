require 'lfs'

local function input_tex()
    local already_loaded = { ['000-main.tex'] = true, ['001-remerciements.tex'] = true }
    for file in lfs.dir[[.]] do
        if lfs.attributes(file, 'mode') == 'file' and file:sub(-4) == '.tex' and not already_loaded[file] then
            local latex_line = string.format([[\input{%s}]], file)
            tex.print(latex_line)
        end
    end
end

return {
    input_tex = input_tex
}